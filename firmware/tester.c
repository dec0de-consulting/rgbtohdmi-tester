// SPDX-License-Identifier: GPL-2.0-or-later

#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>

// Comment out for 60Hz (not been testing in a long time, probably broken now)
#define PAL

static void csync(uint8_t lowtime)
{
    uint8_t i;

    // Wait for timer
    sleep_cpu();

    // Set CSYNC low
    for (i = 0; i < lowtime; i++)
    {
        PORTE = 0b00000000;
        PORTE = 0b00000000;
        PORTE = 0b00000000;
    }

    PORTE = 0b00000010;
}


// Note: some of the maths in here may look "weird" but it compiles to more optimal code than the alternative
void main(void)
{
    int8_t j;
    uint8_t i, k, l;
    uint8_t pb, pc, pd, line;

    cli();

    // configure output pins, overkill but it doesn't matter
    DDRE = 0b11111111;
    DDRD = 0b11111111;
    DDRC = 0b11111111;
    DDRB = 0b11111111;

    // TIMER 1 used for HSYNC
    TCCR1A = 0b00000011;
    TCCR1B = 0b00001000 | 0b00000001;
    TCNT1H = 0;
    TCNT1L = 0;
    TIMSK1 = 0x01;  // 0b00000001;

    TIMSK0 = 0x00;

    // Use sleep function for HSYNC
    sleep_enable();

    sei();

    while(1)
    {
        for (i = 0; i < 3; i++)
        {
            csync(130);  // 3 lines vertical syncs
        }

        for (i = 0; i < 50; i++)
        {
            csync(11);  // 50 lines top blanking
        }

        // 15 lines red vertical pattern
        for (i = 1; i < 16; i++)
        {
            csync(11);
            PORTD = (i << 4);
        }

        // 15 lines red horizontal pattern
        for (i = 0; i < 16; i++)
        {
            uint8_t tmp = 1 << (i / 4);
            uint8_t tmp2 = (tmp << 4) | (tmp >> 4);
            csync(11);
            PORTD = tmp2;
        }
        // 15 lines green vertical pattern
        for (i = 1; i < 16; i++)
        {
            csync(11);
            PORTD = 0b00000000;
            PORTC = i;
        }

        // 15 lines green horizontal pattern
        for (i = 0; i < 16; i++)
        {
            uint8_t tmp = 1 << (i / 4);
            csync(11);
            PORTC = tmp;
        }
        // 15 lines blue vertical pattern
        for (i = 1; i < 16; i++)
        {
            csync(11);
            PORTC = 0b00000000;
            PORTB = i;
        }

        // 15 lines blue horizontal pattern
        for (i = 0; i < 16; i++)
        {
            uint8_t tmp = 1 << (i / 4);
            csync(11);
            PORTB = tmp;
        }

        // Black -> white pattern for 16 lines
        for (i = 1; i < 16; i++)
        {
            csync(11);
            uint8_t tmp = (i << 4) | (i >> 4);
            PORTD = tmp;
            PORTB = PORTC = i;
        }

        PORTC = PORTB = PORTD = 0b00000000;

        // 16 lines white horizontal pattern
        for (i = 0; i < 16; i++)
        {
            uint8_t tmp = 1 << (i / 4);
            uint8_t tmp2 = (tmp << 4) | (tmp >> 4);
            csync(11);
            PORTB = PORTC = tmp;
            PORTD = tmp2;
        }

        // 15 * 6 lines green/red/blue blend pattern
        for (i = 1; i < 16; i++)
        {
            pd = (i * 16);
            pc = (16 - i);
            for (l = 0; l < 5; l++)
            {
                csync(11);
                PORTC = PORTB = PORTD = 0b00000000;
                _delay_us(7);
                PORTD = pd;
                PORTC = pc;
                for (k = 1; k < 16; k++)
                {
                    PORTB = k;
                    _delay_us(2);
                }
                PORTD = 0b00000000;
                PORTC = 0b00000000;
                PORTB = 0b00000000;
            }
        }

        // End of frame
        for (i = 0; i < 8; i++)
        {
          csync(11);
        }

#ifdef PAL

        // Extra 50 lines at the end of PAL
        for (i = 0; i < 50; i++)
        {
            csync(11);
        }

#endif
    }
}


// Wake up CSYNC sleep, nothing needed in the function
ISR(TIMER1_OVF_vect)
{
}
