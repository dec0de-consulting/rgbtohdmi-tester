# Amiga RGBtoHDMI Tester

![Amiga RGBtoHDMI Tester](Tester.png)

This is a testing device for Amiga CPLD RGBtoHDMI boards that does not need an Amiga or Denise chip to generate a test image. It instead uses a microcontroller to generate a test signal. This means you can flash the CPLD and test the board without needing an Amiga at all.

**Note**: This does not generate a clock signal so is not compatible with the non-CPLD RGBtoHDMI boards.

The schematic was made in KiCad nightly 6.99 and can only be opened in a nightly version at the moment.

## BOM

 | Symbol     | Component                            |
 | ---------- | ------------------------------------ |
 | C1, C2, C5 | 0.1uF 0603                           |
 | C3, C4     | 22pF 0603                            |
 | D1         | 0603 red LED                         |
 | R1         | 1M 0603                              |
 | R2         | 10K 0603                             |
 | R3         | 0603 resistor for LED                |
 | J1         | Molex 105017-0001 micro USB B socket |
 | J2, J3     | 36 way 2.54mm card edge connector    |
 | SW1        | CK OS102011MS2QS1 slide switch       |
 | U1         | 48pin DIL ZIF socket                 |
 | U2         | ATMega48PB / 88PB / 168PB / 328PB    |
 | Y1         | 16MHz MQ2 7x5mm XTAL                 |

### Notes

- The ATMega **must** be the 'PB' variant, which has a different pinout to the 'P' and 'PA' variants.
- Tune R3 for a 5v input to the LED D1

## Building

You will need to program the ATMega before soldering it to the board. I use a QFP ZIF socket for this but you can also use Microchip's programming service.

## Microcontroller Flashing

In the `firmware` directory edit the `Makefile` for the ATMega variant you are using. You will need to change the `MCU` and the `LFUSE` as the internal clock circuit is different between the 328PB and the other variants.

You will need `avr-gcc` to compile and `avrdude` to flash. In Linux (and probably macOS), once the `Makefile` has been edited this will be a simple case of `make` to compile and `make install` to flash.

The `Makefile` was setup to use `usbtiny` cable, you may need to change this for your cable.

## Usage

![Test pattern](testpattern.png)

The test pattern as follows:

1. Red full spectrum.
2. Individual red signal bands.
3. Green full spectrum.
4. Individual green signal bands.
5. Blue full spectrum.
6. Individual blue signal bands.
7. Grey / white full spectrum.
8. Individual grey / white signal bands.
9. RGB colour spread pattern.

The full spectrum colours will have unusual banding if a colour line is not working correctly. The individual signal bands will have one missing if a colour line is not working or will show a different colour if it is shorted to another colour line. The white section will likely show off-white if there is a problem. Finally the RGB spread will show unusual horizontal or vertical banding with colour issues.


## Assembled Boards

You can buy ready-assembled RGBtoHDMI testers from the [Retro Supplies store](https://www.retrosupplies.co.uk/amiga-rgbtohdmi/amiga-rgbtohdmi-tester).
